$(document).ready(init);

function init() {
    $(document).scroll(function(){
        if($(this).scrollTop() > 90 ){
            $("header").addClass("fixed")
        }else{
            $("header").removeClass("fixed")
        }
        
        if($(this).scrollTop() > 200 ){
            $(".content").eq(0).addClass("right-c")
            $(".content").eq(1).addClass("left-c")
        }
        
        if($(this).scrollTop() > 500 ){
            $(".content").eq(2).addClass("right-c")
            $(".content").eq(3).addClass("left-c")
        }
        
        if($(this).scrollTop() > 1200 ){
            $(".content").eq(4).addClass("top-1")
        }
        
        if($(this).scrollTop() > 1200 ){
            $(".content").eq(4).addClass("top-1")
        }
        
        if($(this).scrollTop() > 1600 ){
            $(".case").addClass("top-1")
        }
        
        if($(this).scrollTop() > 1600 ){
            $(".contact").addClass("top-1")
        }
        
    })
    
    $(".locate .case div").click(function(){
        var _html = $(this).html()
        $(".popup").show()
        $(".popup .pop-content .message").prepend(_html)
    })
    
    $(".popup .close-popup").click(function(){
        $(".popup").hide()
        $(".popup .pop-content .message").html("")
    })
        
    var nowDate = new Date(),
        year = nowDate.getFullYear()
    $("footer span.year").text(year)
    var _height = $(window).width() * 1.2
    if($(window).width() > 760 && $(window).width() < 1024 && $(window).height() > _height ){
        $(".content").eq(0).addClass("right-c")
        $(".content").eq(1).addClass("left-c")
        
        $(document).scroll(function(){

            if($(this).scrollTop() > 200 ){
                $(".content").eq(2).addClass("right-c")
                $(".content").eq(3).addClass("left-c")
            }

            if($(this).scrollTop() > 600 ){
                $(".content").eq(4).addClass("top-1")
            }

            if($(this).scrollTop() > 800 ){
                $(".content").eq(4).addClass("top-1")
            }

            if($(this).scrollTop() > 1000 ){
                $(".case").addClass("top-1")
            }

            if($(this).scrollTop() > 1100 ){
                $(".contact").addClass("top-1")
            }

        })
        
    }
    
};